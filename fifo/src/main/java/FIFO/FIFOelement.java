package FIFO;

/**
 * Created by mirasomo on 26.05.2017.
 */
public class FIFOelement {
    FIFOelement next;
    Object data;

    public FIFOelement(Object data) {
        this.data = data;
    }

    public void setNext(FIFOelement previous) {
        this.next = previous;
    }

    public FIFOelement getNext() {
        return next;
    }
}
