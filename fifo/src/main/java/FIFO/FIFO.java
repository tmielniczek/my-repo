package FIFO;

/**
 * Created by mirasomo on 26.05.2017.
 */
public class FIFO {
    FIFOelement First;
    FIFOelement lastAdded;
    public void push(Object g){
        FIFOelement f=new FIFOelement(g);
        if(First==null){
            this.First=f;
            this.lastAdded=f;
        }
        else this.lastAdded.setNext(f);
        this.lastAdded=f;

    }

    public void pop(){
        FIFOelement temp;
        temp=this.First;
        this.First=First.getNext();
        temp=null;

    }
}
